package com.test.libon.model;

public enum Instruction {

    G,
    D,
    A;
}

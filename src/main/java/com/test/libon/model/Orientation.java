package com.test.libon.model;

public enum Orientation {
    N,
    S,
    W,
    E;
}

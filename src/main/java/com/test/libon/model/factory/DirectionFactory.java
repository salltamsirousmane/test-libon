package com.test.libon.model.factory;

import com.test.libon.model.*;

public class DirectionFactory {

    private DirectionFactory() {
    }

    public static Direction createDirection(Orientation orientation) {
        switch (orientation) {
            case E:
                return new East();
            case N:
                return new North();
            case S:
                return new South();
            default:
                return new West();

        }
    }
}

package com.test.libon.model;

public class North extends Direction {

    public North() {
        this.x = 0;
        this.y = 1;
        this.orientation = Orientation.N;
    }


    @Override
    public boolean isSwitch(Instruction instruction) {
        if (Instruction.G == instruction) {
            this.context.changeDirection(new West());
        } else if (Instruction.D == instruction) {
            this.context.changeDirection(new East());
        }
        return Instruction.G == instruction || Instruction.D == instruction;
    }


}

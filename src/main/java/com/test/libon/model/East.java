package com.test.libon.model;

public class East extends Direction {

    public East() {
        this.x = 1;
        this.y=0;
        this.orientation = Orientation.E;
    }

    @Override
    public boolean isSwitch(Instruction instruction) {
        if (Instruction.G == instruction) {
            this.context.changeDirection(new North());
        } else if (Instruction.D == instruction) {
            this.context.changeDirection(new South());
        }
        return Instruction.G == instruction || Instruction.D == instruction;
    }
}

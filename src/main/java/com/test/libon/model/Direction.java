package com.test.libon.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class Direction {

    protected int x;
    protected int y;
    protected Orientation orientation;
    protected Engine context;

    /**
     * To know if we will only switch the direction or not
     * @param instruction instruction to process
     */
    protected abstract boolean isSwitch(Instruction instruction);

    /**
     * To know if the movement will not send the machine outside
     * @param instruction instruction to process
     * @param dimension dimension of the area
     */
    protected boolean isPossible(Instruction instruction, Dimension dimension) {
        return context.getX() + x > dimension.getWeight() || context.getY() + y < dimension.getHeight();
    }

    /**
     * To change coordinates to move the machine
     * @param instruction instruction to process
     * @param dimension dimension of the area
     */
    protected void move(Instruction instruction, Dimension dimension) {
        if (!isSwitch(instruction)) {
            if (this.isPossible(instruction, dimension)) {
                context.setX(x + context.getX());
                context.setY(y + context.getY());
            }
        }

    }


}

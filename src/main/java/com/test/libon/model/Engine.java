package com.test.libon.model;

import com.test.libon.model.factory.DirectionFactory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Engine {

    private int x;
    private int y;
    private Direction direction;


    public Engine(Position position) {
        this.x = position.getX();
        this.y = position.getY();
        this.direction = DirectionFactory.createDirection(position.getOrientation());
        this.direction.setContext(this);
    }

    public void changeDirection(Direction direction) {
        this.direction = direction;
        this.direction.setContext(this);
    }

    public void move(Instruction instruction, Dimension dimension) {
        this.direction.move(instruction, dimension);
    }


}

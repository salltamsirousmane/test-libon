package com.test.libon.model;

public class South extends Direction {

    public South() {
        this.x = 0;
        this.y=-1;
        this.orientation = Orientation.S;
    }

    @Override
    public boolean isSwitch(Instruction instruction) {
        if (Instruction.G == instruction) {
            this.context.changeDirection(new East());
        } else if (Instruction.D == instruction) {
            this.context.changeDirection(new West());
        }
        return Instruction.G == instruction || Instruction.D == instruction;
    }

}

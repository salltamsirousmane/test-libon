package com.test.libon.model;

public class West extends Direction {

    public West() {
        this.x = -1;
        this.y=0;
        this.orientation = Orientation.W;
    }

    @Override
    public boolean isSwitch(Instruction instruction) {
        if (Instruction.G == instruction) {
            this.context.changeDirection(new South());
        } else if (Instruction.D == instruction) {
            this.context.changeDirection(new North());
        }
        return Instruction.G == instruction || Instruction.D == instruction;
    }
}

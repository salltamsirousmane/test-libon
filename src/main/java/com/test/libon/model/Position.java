package com.test.libon.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Position {

    private int x;
    private int y;
    Orientation orientation;
}


package com.test.libon.thirdparty.model;

import com.test.libon.model.Dimension;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Payload {

    private Dimension dimension;
    private List<Mower> mowers = new ArrayList<>();
}

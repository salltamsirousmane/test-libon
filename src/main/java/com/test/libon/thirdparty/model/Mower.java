package com.test.libon.thirdparty.model;

import com.test.libon.model.Position;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Mower {

    private Position position;
    private String instructions;
}

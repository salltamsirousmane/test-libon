package com.test.libon;

import com.test.libon.model.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class LibonApplicationTests {

    @Test
    void GAGAGAGAA() {
        //Given
        Position position = Position.builder()
                .x(1)
                .y(2)
                .orientation(Orientation.N)
                .build();
        Engine engine = new Engine(position);

        //When
        engine.move(Instruction.G, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.G, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.G, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.G, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));

        //Then
        assertEquals(1, engine.getX(), "should be equal");
        assertEquals(3, engine.getY(), "should be equal");

    }

    @Test
    void AADAADADDA() {
        //Given
        Position position = Position.builder()
                .x(3)
                .y(3)
                .orientation(Orientation.E)
                .build();
        Engine engine = new Engine(position);

        //When
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.D, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.D, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));
        engine.move(Instruction.D, new Dimension(5, 5));
        engine.move(Instruction.D, new Dimension(5, 5));
        engine.move(Instruction.A, new Dimension(5, 5));


        //Then
        assertEquals(5, engine.getX(), "should be equal");
        assertEquals(1, engine.getY(), "should be equal");
        assertEquals(Orientation.E, engine.getDirection().getOrientation(), "should be equal");
    }

    @Test
    void should_move_north() {
        //Given
        Position position = Position.builder()
                .x(1)
                .y(2)
                .orientation(Orientation.N)
                .build();
        Engine engine = new Engine(position);

        //When
        engine.move(Instruction.A, new Dimension(5, 5));

        //Then
        assertEquals(1, engine.getX(), "should be 1");
        assertEquals(3, engine.getY(), "should be 3");
        assertEquals(Orientation.N, engine.getDirection().getOrientation(), "should be N");


    }

    @Test
    void should_not_move() {
        //Given
        Position position = Position.builder()
                .x(5)
                .y(5)
                .orientation(Orientation.N)
                .build();
        Engine engine = new Engine(position);

        //When
        engine.move(Instruction.A, new Dimension(5, 5));

        //Then
        assertEquals(5, engine.getX(), "should be 5");
        assertEquals(5, engine.getY(), "should be 5");
        assertEquals(Orientation.N, engine.getDirection().getOrientation(), "should be N");

    }

}

# Tondeuse à gazon


L'application est conçue principalement via le design pattern state pour gérer chaque mouvement de manière transparente.

Pour chaque direction prise par la tondeuse, le comportement associé a été implémenté via un polymorphisme.

Les Tests vérifient la conformité avec la spec.

La partie http / api rest n'est pas encore développée.

Prochaines taches : 

- Implémenter le service qui traite les requettes pour envoyer les commandes aux tondeuses ainsi que les controllers.
- Générer le swagger 

Enfin, je souhaiterais décrire la suite de l'implémentation lors de la présentation. 
